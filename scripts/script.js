let data = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function showArray(data, parent = document.body) {
  const ul = document.createElement("ol");

  data.forEach((article) => {
    let li = document.createElement("li");
    li.innerText = article;
    ul.appendChild(li);
  });
  parent.append(ul);
}

showArray(data);

// showArray(data,document.querySelector('#root'));
setTimeout(function () {
  document.querySelector("ol").remove();
}, 3000);

let counter = 3;
const timer = document.createElement("p");
timer.textContent = counter;
document.body.append(timer);
const intervalId = setInterval(function () {
  counter--;
  timer.textContent = counter;
  if (counter === 0) clearInterval(intervalId);
}, 1000);
